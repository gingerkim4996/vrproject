#pragma warning disable IDE0051
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class SwatInputSystemCtrl : MonoBehaviour
{
    private Animator anim;
    private new Transform transform;
    private Vector3 moveDir;


    private void Start()
    {
        anim = GetComponent<Animator>();
        transform = GetComponent<Transform>();
    }

    private void Update()
    {
        if(moveDir!= Vector3.zero)
        {
            transform.rotation = Quaternion.LookRotation(moveDir);

            transform.Translate(Vector3.forward * Time.deltaTime * 4.0f);
            //transform.Translate(Vector3.right * Time.deltaTime * 4.0f);

            //transform.Translate(Vector3.back * Time.deltaTime * 4.0f);

            //transform.Translate(Vector3.left * Time.deltaTime * 4.0f);


        }
    }

    void OnMove(InputValue value)
    {
        Vector2 dir = value.Get<Vector2>();
        Debug.Log($"Move = ({dir.x},{dir.y})");
        moveDir = new Vector3(dir.x, 0f, dir.y);
        //anim.SetFloat("PosY", dir.magnitude);
        //anim.SetFloat("PosX", dir.magnitude);
        anim.SetFloat("Move", dir.magnitude);
    }

    void OnSprint()
    {
        
    }

}

#pragma warning disable IDE0051
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerInputSystemCtrl : MonoBehaviour
{
    private Animation anim;
    private new Transform transform;
    private Vector3 moveDir;

    private void Start()
    {
        anim = GetComponent<Animation>();
        transform = GetComponent<Transform>();
        anim.Play("Idle");
    }

    void OnMoove(InputValue value)
    {
        Vector2 dir = value.Get<Vector2>();
        Debug.Log($"Moove = ({dir.x},{dir.y})");
        moveDir = new Vector3(dir.x, 0f, dir.y);
    }

    void Update()
    {
        if (moveDir != Vector3.zero)
        {
            transform.rotation = Quaternion.LookRotation(moveDir);
            transform.Translate(Vector3.forward * Time.deltaTime * 4.0f);

        }
    }
}

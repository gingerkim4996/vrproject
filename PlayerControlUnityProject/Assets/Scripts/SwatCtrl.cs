using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwatCtrl : MonoBehaviour
{
    float h, v, r;
    Animator animator;
    Transform tr;
    public float moveSpeed = 5.0f; // 움직이는 속도
    public float rotSpeed = 90f; // 회전하는 속도

    void Start()
    {
        animator = GetComponent<Animator>();
        tr = gameObject.transform;//gameObject 대신 this를 쓰기도 함
    }

    void Update()
    {
        h = Input.GetAxis("Horizontal");
        v = Input.GetAxis("Vertical");
        r = Input.GetAxis("Mouse X");
        tr.Translate(Vector3.right * h * moveSpeed * Time.deltaTime); // Time.deltatime은 부드럽게 하기 위해
        {
            animator.SetFloat("PosX", h, 0.1f, Time.deltaTime);
        }
        tr.Translate(Vector3.forward * v * moveSpeed * Time.deltaTime);
        {
            animator.SetFloat("PosY", v, 0.01f, Time.deltaTime);
        }
        tr.Rotate(Vector3.up * r * rotSpeed * Time.deltaTime);
    }
}

#pragma warning disable IDE0051
// 함수를 선언하고 호출하지 않으면 경고가 뜨는 것을 막는 코드
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerCtrl : MonoBehaviour
{
    private Animator anim;
    private new Transform transform;
    private Vector3 moveDir;

    private PlayerInput playerInput;
    private InputActionMap mainActionMap;
    private InputAction moveAction;
    private InputAction attackAction;

    private void Start()
    {
        anim = GetComponent<Animator>();
        transform = GetComponent<Transform>();
        playerInput =GetComponent<PlayerInput>();

        mainActionMap = playerInput.actions.FindActionMap("PlayerActions");

        moveAction = playerInput.actions.FindAction("Move");
        attackAction = mainActionMap.FindAction("Attack");

        // Move 액션의 performed 이벤트 연결
        moveAction.performed += ctx =>
        {
            Vector2 dir = ctx.ReadValue<Vector2>();
            moveDir = new Vector3(dir.x, 0f, dir.y);
            anim.SetFloat("Movement", dir.magnitude);
        };

        // Move 액션의 Canceled 이벤트 연결
        moveAction.canceled += ctx =>
        {
            moveDir = Vector3.zero;
            anim.SetFloat("Movement", 0.0f);
        };

        // Attack 액션의 performed 이벤트 연결
        attackAction.performed += ctx =>
        {
            Debug.Log("Attack by C# Event");
            anim.SetTrigger("Attack");
        };

    }

    private void Update()
    {
        if(moveDir != Vector3.zero)
        {   // 키보드의 입력한 진행 방향으로 회전
            transform.rotation = Quaternion.LookRotation(moveDir);

            // 회전한 후 전진 방향으로 이동
            transform.Translate(Vector3.forward * Time.deltaTime *4.0f);
        }
    }

    #region SEND_MESSAGE
    // PlayerInput 컴포넌트가 있는 게임 오브젝트에 SendMessage 함수를 이용해 호출한다.
    // On만 붙여도 호출이 가능하다
    void OnMove(InputValue value)
    {
        Vector2 dir = value.Get<Vector2>();
        moveDir = new Vector3(dir.x, 0f, dir.y);
        anim.SetFloat("Movement", dir.magnitude);
    }

    void OnAttack()
    {
        anim.SetTrigger("Attack");
    }
    #endregion

    #region UNITY_EVENTS
    // 액션별로 이벤트 연결 인터페이스가 새성되고 각각 호출하려는 함수를 연결해 사용한다.
    public void OnMove(InputAction.CallbackContext ctx)
    {
        Vector2 dir = ctx.ReadValue<Vector2>();
        moveDir = new Vector3(dir.x, 0f, dir.y);
        anim.SetFloat("Movement", dir.magnitude);
    }
    public void OnAttack(InputAction.CallbackContext ctx)
    {
        Debug.Log($"ctx.phase = {ctx.phase}");

        if(ctx.performed) //실행 중일때
        {
            Debug.Log("Attack");
            anim.SetTrigger("Attack");
        }
        // Invoke Unity Events 옵션을 사용해 연결한 함수는
        // 총 3번 호출 된다. 즉 InputAction에 정의한 액션은
        // 시작 실행(Perfomed) 취소(Lost) CallBack함수를 각각 한번씩 호출하며
        // 어떤 상태로 호출됐는지에 대한 정보는 
        // CallbackContext.phase 속성을 통해서 알 수 있다.
    }


    #endregion
}

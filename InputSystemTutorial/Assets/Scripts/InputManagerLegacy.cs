using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManagerLegacy : MonoBehaviour
{
    float h = 0f;
    float v = 0f;
    Transform tr;
    Animator animator;

    // Start is called before the first frame update
    void Start()
    {
        tr = GetComponent<Transform>();
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        h = Input.GetAxis("Horizontal");
        v = Input.GetAxis("Vertical");

        /* 유니티2019부터는 인풋시스템을 사용 중임
        인풋매니저는 다양한 플랫폼을 지원 이전에 설계되었기 때문에
        확장성과 편의성이 부족하다
        기기마다 새로운 설정을 했어야했음

        인풋시스템은 다양한 플랫폼을 지원하며
        확장성과 편의성이 좋아 앞으로는 인풋시스템을 사용하자


        인풋시스템은 void update에서 작동하지 않아도 돼서
        update에서 작동하면 프레임 마다 확인하기 때문에 메모리를 아낄수 있음
        
        인풋매니저를 사용하되 인풋시스템도 사용하여 장단점을 알고 있어야함*/

        // 합쳐서 할 때
        Vector3 movedir = (h * Vector3.right) + (v * Vector3.forward);

        /*
        앞 뒤 분리해서 할 때
        tr.Translate(Vector3.forward * v * Time.deltaTime * 5f);
        tr.Translate(Vector3.right * h * Time.deltaTime * 5f);
        */

        animator.SetFloat("Movement", movedir.magnitude);
        if (movedir != Vector3.zero)
        {   // 키보드의 입력한 진행 방향으로 회전
            tr.rotation = Quaternion.LookRotation(movedir);

            // 회전한 후 전진 방향으로 이동
            // tr.Translate(Vector3.forward * Time.deltaTime * 4.0f);
            tr.Translate(Vector3.forward * Time.deltaTime * 5.0f);
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            animator.SetTrigger("Attack");
        }
    }
}

#pragma warning disable IDE0051
// 함수를 선언하고 호출하지 않으면 경고가 뜨는 것을 막는 코드
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem; // <- InputSystem이란 네임스페이스
// using의 회색은 안쓰고 있다는 뜻 쓰고 있다면 흰색

public class PlayerCtrlByEvent : MonoBehaviour
{
    private InputAction moveAction;
    private InputAction attackAction;
    private Animator anim;
    private Vector3 moveDir;


    void Start()
    {
        anim = GetComponent<Animator>();
        // move 액션 생성 및 타입 설정
        moveAction = new InputAction("Move",InputActionType.Value); // 밸류는 누르는 값만큼 받아옴
        // move 액션의 복합 바인딩 정보 정의
        moveAction.AddCompositeBinding("2DVector") // 메인액션에서 컴포지트 타입이 2D 벡터였음 그걸 그대로 스크립트로 적은거임
            .With("Up", "<Keyboard>/w")
            .With("Down", "<Keyboard>/s")
            .With("Right", "<Keyboard>/a")
            .With("Left", "<Keyboard>/d");
        // move 액션의 performed canceled 이벤트 연결
        moveAction.performed += ctx =>
        {
            Vector2 dir = ctx.ReadValue<Vector2>();
            moveDir = new Vector3(dir.x, 0f, dir.y);
            anim.SetFloat("Movement", dir.magnitude);
        };
        moveAction.canceled += ctx =>
        {
            moveDir = Vector3.zero;
            anim.SetFloat("Movement", 0f);
        };
        // Move 액션 활성화
        moveAction.Enable();
        // Attack 액션의 활성화
        attackAction = new InputAction("Attack", InputActionType.Button,"<Keyboard>/space");
        // Attack 액션의 Performed 이벤트 연결
        attackAction.performed += ctx =>
        {
            anim.SetTrigger("Attack");
        };
        // Attack 액션 활성화
        attackAction.Enable();
    }

    void Update()
    {
        if(moveDir != Vector3.zero)
        {
            transform.rotation = Quaternion.LookRotation(moveDir);
            transform.Translate(Vector3.forward * Time.deltaTime * 4.0f);
        }

    }
}

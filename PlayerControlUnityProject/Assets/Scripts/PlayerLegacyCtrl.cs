using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLegacyCtrl : MonoBehaviour
{
    public Animation animation;
    private float h, v;
    private Vector3 moveDir;
    public float Speed = 5.0f;

    void Start()
    {
        animation = GetComponent<Animation>();
        animation.Play("Idle");
    }

    void Update()
    {
        h = Input.GetAxis("Horizontal");
        v = Input.GetAxis("Vertical");
        moveDir = (h * Vector3.right) + (v * Vector3.forward);
        transform.Translate(moveDir.normalized * Time.deltaTime * Speed);
        // normalized 정규화 - > 잘못된 데이터를 고친다는 의미 양방향으로 힘이 가더라도 합이 되지 않고 일정하게 되게끔 함

        #region 애니메이션 연동
        if(h > 0.1f) // 양수에 해당하는 숫자로 Horzontal 기준 오른쪽임.
        {
            animation.CrossFade("RunR", 0.3f);
            // 크로스 페이드 : 직전 동작 지금 동작하는 애니메이션을
            // 0.3초 동안 혼합해서 부드러운 애니메이션을 만든다.
        }

        else if(h < -0.1f) // 음수에 해당하는 숫자로 Horizontal 기준 왼쪽임.
        {
            animation.CrossFade("RunL", 0.3f);
        }

        else if(v > 0.1f) // 양수에 해당하는 숫자로 Vertical 기준 앞임
        {
            animation.CrossFade("RunF", 0.3f);
        }

        else if(v < -0.1f) // 음수에 해당하는 숫자로 Vertical 기준 뒤임
        {
            animation.CrossFade("RunB", 0.3f);
        }

        else // 위에 해당되지 않을시에
        {
            animation.CrossFade("Idle", 0.3f);
        }

        if(Input.GetKey(KeyCode.LeftShift) && Input.GetKey(KeyCode.W)) // 왼쪽 쉬프트와 w를 동시에 눌렀다면
        {
            animation.CrossFade("SprintF", 0.3f); // 크로스페이드로 sprintf를 
            Speed = 10f;
        }
        else if(Input.GetKeyUp(KeyCode.LeftShift))
        {
            Speed = 5f;
        }

        #endregion
    }
}

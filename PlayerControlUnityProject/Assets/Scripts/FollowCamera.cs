using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCamera : MonoBehaviour
{   // 선언
    [SerializeField] private Transform camTr; // 카메라 위치
    // attribute 퍼블릭처럼 보이나 외부에선 접근할 수 없음
    // 주석은 개발자가 적고 개발자가 읽으나 어트리뷰트는 개발자가 적고 컴파일러가 읽는다
    // 시리얼라이즈 필드 같은 경우 보이게는 해주나 외부에선 접근할 수 없다.
    // 정보 은닉을 위해 private를 사용하나 시리얼라이즈 필드로 보이게 하고 나중에 지우면 됨
    [SerializeField] private Transform target; // 여기서 타겟은 플레이어임
    [SerializeField] [Range(2f,20f)] private float height = 7f; // 카메라 높이    Range는 범위를 조절할 수 있게 해줌
    [SerializeField] [Range(2f,20f)] private float distance = 5f; // 카메라 거리  Range는 범위를 조절할 수 있게 해줌
    float damping = 10f; // 댐핑은 부드럽게 해준다는 의미로 카메라가 안흔들리게 부드럽게 해주는 것

    // 초기화
    void Start() // 게임 시작 전에 
    {
        camTr = transform; // 자기 자신의 위치
        target = GameObject.FindWithTag("Player").transform;
        // 대문자인 GameObject는 하이어라키에서 찾는다는 의미고 소문자인 gameObject는 자기 자신을 말함
        // WithTag가 안붙을 경우 그냥 오브젝트의 이름만을 찾으나 WithTag가 붙을 경우 태그가 ~인 것을 찾는다.
    }

    void Update()
    {

        Vector3 pos = target.position - (Vector3.forward * distance) + (Vector3.up * height);
        // 캠tr의 포지션이 타겟의 포지션으로 넘어가게 됨
        // 타겟의 distance만큼의 거리와 height만큼의 높이로 이동하게 됨
        // 카메라 위치 = 타겟 위치 - 타겟과의 전방 거리 + 높이(카메라는 타겟의 뒤에 있기 때문에 그만큼 마이너스)

        // 이하 보간함수 -> 애니메이션의 중간이 끊겨보일때 사용(Slerp)
        camTr.position = Vector3.Slerp(camTr.position, pos, Time.deltaTime * damping);
                                     //from camTr 카메라의 시작 위치
                                     //to   pos 이후 목표 위치, t 카메라의 흔들리는 시간만큼
                                     //Vector3.Slerp는 곡면보간 함수이나 Vector3.Lerp는 선형보간 함수임
        camTr.LookAt(target.position);
        // 카메라가 타겟(주인공이자 주인공의 위치)을 쳐다본다.
    }
}
